XMLDiffPatchGUI
===============

This is "The XML Diff and Patch GUI Tool" that was published on MSDN [http://msdn.microsoft.com/en-us/library/aa302295.aspx](http://msdn.microsoft.com/en-us/library/aa302295.aspx). Only source code was ever published so this repo is here to recify that with a binary and installer.

The original source code has also been integrated into [XML Notepad 2007](http://xmlnotepad.codeplex.com "http://xmlnotepad.codeplex.com"), however, XML Notepad 2007 hasn't included all the original options when doing the diff, so you have to use the default choices that are set for you.

The project files should work in [Sharp Develop](http://www.icsharpcode.net/opensource/sd/) as well as [Visual Studio](http://www.microsoft.com/visualstudio/eng/), although I have only tried the former.

The build is from within the IDE and will automatically create the exe and an installer. This assumes that you have [Inno Setup](http://www.jrsoftware.org/isinfo.php) installed at  
>C:\Program Files (x86)\Inno Setup 5\Compil32.exe

The icon was downloaded from <http://www.iconfinder.com/icondetails/45246/256/align_left_icon>  
It was modified with GIMP <http://www.gimp.org/>  
It was converted to a multi-file icon file with <http://www.softicons.com/icon-tools/icon-converter>


